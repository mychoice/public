/*
Copyright INRAE
Contact contributor(s) : Rallou Thomopoulos / Julien Cufi (26/03/2020)
MyChoice is a web application supporting collective decision.
See more on https://ico.iate.inra.fr/MyChoice
This application is registered to the European organization for the
protection of authors and publishers of digital creations with
the following identifier: IDDN.FR.001.280002.000.R.P.2020.000.20900 

This software is governed by the CeCILL-C license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL-C
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 
As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 
In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 
The fact that you are presently reading this means that you have had
knowledge of the CeCILL-C license and that you accept its terms.
*/
import { state, getFilteredItems } from "@/store";
import { NormalizedArgument, NormalizedObject } from "@/@types";
import { computed } from "@vue/composition-api";

export const getCriterions = function() {
  const criterions = state.data!.criterions;
  // if (state.mode !== "consensus") {
  //   return filterNormalizedObjectByItemProp(criterions, "criterion");
  // }
  return criterions;
};
export const c_criterions = computed(() => state.data!.criterions);

export const getCriterionsAsArray = function() {
  const criterions = Object.keys(getCriterions());

  return criterions.map(criterion => Number(criterion));
};
export const criterionsIds = function() {
  return Object.keys(getCriterions()).map(id => Number(id));
};
export const c_criterionsIds = computed(() => {
  return Object.keys(c_criterions.value).map(id => Number(id));
});
export const getCriterionById = function(criterionId: number) {
  return getCriterions()[criterionId];
};

export const getCriterionName = function(criterionId: number | string) {
  return getCriterions()[Number(criterionId)].name;
};

export const getFilteredCriterions = () => {
  return criterionsIds().filter(criterionId => {
    return getFilteredItems().some(item => {
      return item.criterion === criterionId;
    });
  });
};
export const c_filteredCriterions = computed(() => {
  return c_criterionsIds.value.filter(criterionId => {
    return getFilteredItems().some(item => {
      return item.criterion === criterionId;
    });
  });
});
